import 'package:flutter/material.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:get/get.dart';
import 'package:monica/quill/quillEditor_controller.dart';

import '../neu/ui/neu_ui.dart';

class QuillEditorPage extends StatelessWidget {
  const QuillEditorPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    QuillEditorController controller = Get.put(QuillEditorController());
    return Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            QuillToolbar.basic(
                controller: controller.getController(),
                showAlignmentButtons: true,
                customButtons: [
                  QuillCustomButton(
                      icon: Icons.ac_unit,
                      onTap: () {
                        debugPrint('snowflake1');
                      }),
                ]),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: NeuContainer(
                  padding: const EdgeInsets.all(8),
                  depth: -1,
                  child: QuillEditor.basic(
                    controller: controller.getController(),
                    readOnly: false, // true for view only mode
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
