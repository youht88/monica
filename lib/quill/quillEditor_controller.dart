import 'package:get/get.dart';
import 'package:flutter_quill/flutter_quill.dart';

class QuillEditorController extends GetxController {
  QuillController _controller = QuillController.basic();
  QuillController getController() {
    return _controller;
  }
}
