import 'package:flutter/material.dart';
//import 'package:babylonjs_viewer/babylonjs_viewer.dart';

class View3dPage extends StatelessWidget {
  const View3dPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("view3d")),
      body: Center(
        child: Container(
          color: Colors.red,
          width: 300,
          height: 300,
          // child: BabylonJSViewer(
          //   src: 'https://models.babylonjs.com/boombox.glb',
          // ),
        ),
      ),
    );
  }
}
