// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:nine_grid_view/nine_grid_view.dart';

// import 'image_picker_controller.dart';

// class ImagePickerPage extends StatelessWidget {
//   final MyImagePickerController controller = Get.put(MyImagePickerController());

//   ImagePickerPage({Key? key}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(),
//       body: Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: Column(children: [
//           Row(
//             children: [
//               ElevatedButton(
//                   onPressed: () async {
//                     await controller.selectImages();
//                   },
//                   child: const Text("选多张照片")),
//               const SizedBox(width: 8),
//               ElevatedButton(
//                   onPressed: () async {
//                     await controller.selectVideos();
//                   },
//                   child: const Text("视频")),
//             ],
//           ),
//           GetBuilder<MyImagePickerController>(
//             builder: (_) {
//               return Container(
//                 child: controller.listImagePaths.isNotEmpty
//                     ? Wrap(
//                         spacing: 4,
//                         runSpacing: 4,
//                         children: [
//                           for (var item in controller.listImagePaths)
//                             Image.file(File(item.path!),
//                                 width: 50, height: 50, fit: BoxFit.cover)
//                         ],
//                       )
//                     : Container(),
//               );
//             },
//           ),
//           const SizedBox(height: 20),
//           GetBuilder<MyImagePickerController>(
//             builder: (_) {
//               return Container(
//                 child: controller.listImagePaths.isNotEmpty
//                     ? NineGridView(
//                         width: 150,
//                         height: 150,
//                         arcAngle: 10,
//                         type: NineGridType.weChatGp,
//                         itemCount: controller.listImagePaths.length,
//                         itemBuilder: (context, idx) {
//                           return Image.file(
//                               File(controller.listImagePaths[idx].path!),
//                               width: 50,
//                               height: 50,
//                               fit: BoxFit.cover);
//                         })
//                     : Container(),
//               );
//             },
//           ),
//           const SizedBox(height: 8),
//           GetBuilder<MyImagePickerController>(
//             builder: (_) {
//               return Container(
//                 child: controller.listImagePaths.isNotEmpty
//                     ? Wrap(
//                         spacing: 4,
//                         runSpacing: 4,
//                         children: [
//                           for (var item in controller.listImagePaths)
//                             Text("${item.galleryMode!.index}")
//                         ],
//                       )
//                     : Container(),
//               );
//             },
//           )
//         ]),
//       ),
//     );
//   }
// }
