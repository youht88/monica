// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:image_pickers/image_pickers.dart';

// class MyImagePickerController extends GetxController {
//   List<Media> listImagePaths = [];

//   Future<void> selectImages() async {
//     listImagePaths = await ImagePickers.pickerPaths(
//         galleryMode: GalleryMode.image,
//         selectCount: 12,
//         showGif: false,
//         showCamera: true,
//         compressSize: 500,
//         uiConfig: UIConfig(uiThemeColor: const Color(0xffff0f50)),
//         cropConfig: CropConfig(enableCrop: false, width: 2, height: 1));
//     update();
//   }

//   Future<void> selectVideos() async {
//     listImagePaths = await ImagePickers.pickerPaths(
//         galleryMode: GalleryMode.video,
//         selectCount: 3,
//         showGif: false,
//         showCamera: true,
//         compressSize: 500,
//         uiConfig: UIConfig(uiThemeColor: const Color(0xffff0f50)),
//         cropConfig: CropConfig(enableCrop: false, width: 2, height: 1));
//     update();
//   }
// }
