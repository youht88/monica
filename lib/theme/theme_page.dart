import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import 'theme_controller.dart';

class ThemePage extends StatelessWidget {
  const ThemePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeController controller = Get.put(ThemeController());
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GFButtonBar(children: [
            GFButton(
              text: "system",
              onPressed: () async {
                await controller.systemTheme();
              },
            ),
            GFButton(
              text: "light",
              colorScheme: const ColorScheme.light(),
              onPressed: () async {
                await controller.lightTheme();
              },
            ),
            GFButton(
              text: "dark",
              colorScheme: const ColorScheme.dark(),
              onPressed: () async {
                await controller.darkTheme();
              },
            )
          ]),
        ),
      ),
    );
  }
}
