import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:get/get.dart';

class ThemeController extends GetxController {
  systemTheme() async {
    Get.changeThemeMode(ThemeMode.system);
    if (Get.isPlatformDarkMode) {
      Get.changeTheme(ThemeData.dark());
      await Future.delayed(
          const Duration(microseconds: 250), () => Get.forceAppUpdate());
    } else {
      Get.changeTheme(ThemeData.light());
      await Future.delayed(
          const Duration(microseconds: 250), () => Get.forceAppUpdate());
    }
  }

  lightTheme() async {
    Get.changeThemeMode(ThemeMode.light);
    Get.changeTheme(ThemeData.light());
    NeumorphicTheme.of(Get.context!)!.themeMode = ThemeMode.light;

    // await Future.delayed(
    //     const Duration(microseconds: 250), () => Get.forceAppUpdate());
  }

  darkTheme() async {
    Get.changeThemeMode(ThemeMode.dark);
    Get.changeTheme(ThemeData.dark());
    NeumorphicTheme.of(Get.context!)!.themeMode = ThemeMode.dark;
    // await Future.delayed(
    //     const Duration(microseconds: 250), () => Get.forceAppUpdate());
  }
}
