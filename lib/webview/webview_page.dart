import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:monica/webview/webview_controller.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatelessWidget {
  const WebViewPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyWebViewController controller = Get.put(MyWebViewController());
    return Scaffold(
        appBar: AppBar(),
        body: Column(
          children: [
            GestureDetector(
                onTap: () {}, child: Center(child: Text("webview"))),
            Expanded(child: WebView(initialUrl: "www.baidu.com"))
          ],
        ));
  }
}
