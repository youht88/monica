import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';
//import 'package:monica/alarm/alarm_page.dart';
import 'package:monica/crypto/crypto_page.dart';

import 'package:monica/localAuth/local_auth_page.dart';
import 'package:monica/quill/quillEditor_page.dart';
import 'package:monica/shader/shader_page.dart';
import 'package:monica/socket_server/socket_server_page.dart';
import 'package:monica/svg/svg_page.dart';
import 'package:monica/test_get/test_get.dart';
import 'package:monica/webview/webview_page.dart';
import 'package:monica/ws_server/ws_server_page.dart';
import 'package:monica/xterm/xterm_page.dart';
import 'package:monica/zwidget/zwidget_paget.dart';

import 'blobs/blobs_page.dart';
import 'chat/chat_page.dart';
import 'contacts/contacts_page.dart';
import 'doubleBlind/buy/db_buy_page.dart';
import 'echart/echart_page.dart';
import 'getWidget/getwidget_page.dart';
import 'hero/hero_page.dart';
import 'homepage_controller.dart';
import 'imagePicker/image_picker_page.dart';
import 'jdenticon/jdenticon_page.dart';
import 'neu/neu_page.dart';
import 'ninePic/ninePic_page.dart';
import 'picker/picker_page.dart';
import 'puppeteer/puppeteer_page.dart';
import 'theme/theme_page.dart';
import 'view3d/view3d_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final HomePageController controller = Get.put(HomePageController());
    return Scaffold(body: ChatPage());
    // return Scaffold(
    //     appBar: AppBar(title: const Text('HomePagePage')),
    //     body: GetBuilder<HomePageController>(
    //       builder: (_) {
    //         return SafeArea(child: _body(controller));
    //       },
    //     ));
  }

  Widget _body(HomePageController controller) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          Wrap(spacing: 4, runSpacing: 4, children: [
            gfButton(controller, msg: "clear"),
            gfButton(controller,
                msg: "SocketServer", page: const SocketServerPage()),
            gfButton(controller, msg: "Local Auth", page: LocalAuthPage()),
            //gfButton(controller, msg: "Image Picker", page: ImagePickerPage()),
            gfButton(controller, msg: "GetWidget", page: GetWidgetPage()),
            gfButton(controller, msg: "Buy", page: DBBuyPage()),
            gfButton(controller, msg: "Echart", page: EchartPage()),
            gfButton(controller, msg: "Blob", page: const BlobPage()),
            gfButton(controller, msg: "Theme", page: const ThemePage()),
            gfButton(controller, msg: "Ninepic", page: const NinepicPage()),
            gfButton(controller, msg: "Jdenticon", page: const JdenticonPage()),
            gfButton(controller, msg: "Contacts", page: const ContactsPage()),
            gfButton(controller, msg: "Picker", page: const PickerPage()),
            gfButton(controller, msg: "view3d", page: const View3dPage()),
            gfButton(controller,
                msg: "quillEditor", page: const QuillEditorPage()),
            gfButton(controller, msg: "hero", page: const HeroPage()),
            gfButton(controller, msg: "svg", page: const SvgPage()),
            gfButton(controller, msg: "neumorphic", page: const NeuPage()),
            gfButton(controller, msg: "shader", page: ImageShaderDemo()),
            gfButton(controller, msg: "crypto", page: CryptoPage()),
            gfButton(controller, msg: "zwidget", page: ZwidgetPage()),
            gfButton(controller, msg: "testGet", page: TestGetPage()),
            gfButton(controller, msg: "Xterm", page: XtermPage()),
            gfButton(controller, msg: "webview", page: WebViewPage()),
            //gfButton(controller, msg: "alarm", page: AlarmPage()),
            gfButton(controller, msg: "wsServer", page: WsServerPage()),
            gfButton(controller, msg: "puppeteer", page: PuppeteerPage()),
            gfButton(controller, msg: "chat", page: ChatPage()),
          ]),
          // Row(
          //   children: const [
          //     GFAvatar(size: GFSize.SMALL),
          //     GFAvatar(size: GFSize.MEDIUM),
          //     GFAvatar(size: GFSize.LARGE),
          //   ],
          // ),
          // Row(
          //   children: const [
          //     GFBadge(text: "消息"),
          //   ],
          // ),
          // const GFLoader(
          //   type: GFLoaderType.custom,
          //   loaderIconOne: Text('Please'),
          //   loaderIconTwo: Text('Wait'),
          //   loaderIconThree: Text('a moment'),
          // ),
          Expanded(
              child: ListView(
                  children: controller.event
                      .map<Text>((String item) => Text(item))
                      .toList()))
        ]),
      );

  GFButton gfButton(HomePageController controller,
      {required String msg, Widget? page}) {
    return GFButton(
        onPressed: () async {
          if (msg == "clear") {
            controller.event.clear();
            controller.update();
          } else {
            controller.event.add(msg);
            controller.update();
            await Get.to(page);
          }
        },
        child: Text(msg));
  }
}
