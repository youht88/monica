import 'package:blobs/blobs.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'blobs_controller.dart';

class BlobPage extends StatelessWidget {
  const BlobPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlobsController controller = Get.put(BlobsController());
    return Scaffold(body: GetBuilder<BlobsController>(
      builder: (_) {
        return GestureDetector(
            onTap: () {
              controller.updateBlobId();
            },
            child: Center(
                child: Blob.animatedFromID(
                    controller: controller.ctl,
                    id: [controller.blobId],
                    size: 200)));
      },
    ));
  }
}
