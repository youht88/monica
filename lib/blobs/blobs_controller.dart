import 'dart:developer';

import 'package:blobs/blobs.dart';
import 'package:get/get.dart';

class BlobsController extends GetxController {
  String blobId = "";
  int i = 1;
  BlobController ctl = BlobController();
  @override
  onInit() {
    super.onInit();
    updateBlobId();
  }

  updateBlobId() {
    i++;
    blobId = '3-3-$i';
    log(blobId);
    BlobData data = ctl.change();
    data.id = '3-3-3399';
    log("${data.edges},${data.id},${data.growth},${data.curves?.start},${data.points?.id},${data.path},${data.svgPath}");
    update();
  }
}
