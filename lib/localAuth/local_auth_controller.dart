import 'dart:developer';

import 'package:get/get.dart';
import 'package:local_auth/local_auth.dart';

class LocalAuthController extends GetxController {
  Future auth() async {
    var localAuth = LocalAuthentication();
    final biometricList = await localAuth.getAvailableBiometrics();
    log("${[for (BiometricType item in biometricList) item.index]}");
    await localAuth.authenticate(localizedReason: "验证个人信息以重新进入程序！");
  }
}
