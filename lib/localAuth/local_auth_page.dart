import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:monica/localAuth/local_auth_controller.dart';

class LocalAuthPage extends StatelessWidget {
  final LocalAuthController controller = Get.put(LocalAuthController());

  LocalAuthPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          Row(
            children: [
              ElevatedButton(
                  onPressed: () async {
                    await controller.auth();
                  },
                  child: const Text("auth")),
              const SizedBox(width: 8),
              ElevatedButton(onPressed: () async {}, child: const Text("auth")),
            ],
          ),
          GetBuilder<LocalAuthController>(
            builder: (_) {
              return Container();
            },
          )
        ]),
      ),
    );
  }
}
