import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import 'picker_controller.dart';

class PickerPage extends StatelessWidget {
  const PickerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PickerController controller = Get.put(PickerController());
    return Scaffold(
        appBar: AppBar(),
        body: GetBuilder<PickerController>(
          builder: (_) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                GestureDetector(
                    onTap: () {
                      controller.addressPick(context);
                    },
                    child: Text(
                        "地址:${controller.initProvince} - ${controller.initCity} - ${controller.initTown}")),
                GestureDetector(
                    onTap: () {
                      controller.singlePick(context);
                    },
                    child: Text("学历:${controller.initData}")),
                GestureDetector(
                    onTap: () {
                      controller.multiPick(context);
                    },
                    child: Text("时间:${controller.timeData2Select}")),
                GestureDetector(
                    onTap: () {
                      controller.datePicker(context);
                    },
                    child: Text("duration:${controller.pDuration.toString()}")),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GFButtonBadge(
                      onPressed: () {
                        controller.json5parse();
                      },
                      text: "json5"),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("${controller.json5stringify()}"),
                )
              ],
            ),
          ),
        ));
  }
}
