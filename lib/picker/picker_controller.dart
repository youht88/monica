import 'dart:developer';

import 'package:flutter_pickers/more_pickers/init_data.dart';
import 'package:flutter_pickers/style/default_style.dart';
import 'package:flutter_pickers/time_picker/model/date_mode.dart';
import 'package:flutter_pickers/time_picker/model/pduration.dart';
import 'package:flutter_pickers/time_picker/model/suffix.dart';
import 'package:get/get.dart';
import 'package:flutter_pickers/pickers.dart';
import 'package:json5/json5.dart';

class PickerController extends GetxController {
  String initData = '请选择';
  List? timeData2Select = [];
  PDuration? pDuration;
  String initProvince = '四川省', initCity = '成都市', initTown = '双流区';
  final timeData = [
    ['上午', '下午'],
    List.generate(12, (index) => (index + 1).toString()).toList(),
    List.generate(60, (index) => index.toString()).toList(),
    List.generate(60, (index) => index.toString()).toList(),
  ];
  dynamic parsed;
  @override
  onInit() {
    super.onInit();
  }

  addressPick(context) {
    Pickers.showAddressPicker(
      context,
      initProvince: initProvince,
      initCity: initCity,
      initTown: initTown,
      onConfirm: (p, c, t) {
        initProvince = p;
        initCity = c;
        initTown = t!;
        update();
      },
    );
  }

  datePicker(context) {
    Pickers.showDatePicker(
      context,
      // 模式，详见下方
      mode: DateMode.YMD,
      // 后缀 默认Suffix.normal()，为空的话Suffix()
      suffix: Suffix(),
      // 样式  详见下方样式
      pickerStyle: DefaultPickerStyle(haveRadius: true, title: "出生日期"),
      // 默认选中
      selectDate: PDuration(hour: 18, minute: 36, second: 36),
      minDate: PDuration(hour: 12, minute: 38, second: 3),
      maxDate: PDuration(hour: 12, minute: 40, second: 36),
      onConfirm: (p) {
        log('longer >>> 返回数据：$p');
        pDuration = p;
        update();
      },
      // onChanged: (p) => print(p),
    );
  }

  multiPick(context) {
    Pickers.showMultiPicker(
      context,
      data: timeData,
      selectData: timeData2Select,
      suffix: ['', '时', '分', '秒'],
      onConfirm: (p, idx) {
        timeData2Select = p.map((x) => x).toList();
        update();
      },
    );
  }

  singlePick(context) {
    Pickers.showSinglePicker(context,
        //data: ['PHP', 'JAVA', 'C++', 'Dart', 'Python', 'Go'],
        data: PickerDataType.education,
        selectData: initData,
        onConfirm: (p, position) {
          initData = p;
          update();
        },
        onChanged: (p, _) => log('数据发生改变：$p'));
  }

  json5parse() {
    String script = """
     hello
     I am a student.
""";
    script = script.replaceAll("\n", "\\n");

    var str = """{
      'a': 1,
      "b": [
        1,
        3,
        5,
      ],
      "c": "$script",
      "d": "hi let's 
           go"
    }
    """;
    //str = str.replaceAll("\n", "\\n");
    parsed = JSON5.parse(str);
    update();
  }

  json5stringify() {
    return JSON5.stringify(parsed);
  }
}
