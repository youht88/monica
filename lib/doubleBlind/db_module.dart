class DBProduct {
  String id;
  String name;
  String note;
  String pic;
  num price;
  num rate;
  int nonce;
  List<String> detailPic = [];
  DBProduct(
      {required this.id,
      required this.name,
      required this.note,
      required this.pic,
      required this.price,
      required this.rate,
      required this.nonce});
}
