import 'package:get/get.dart';

import '../db_module.dart';

class DBBuyController extends GetxController {
  List<DBProduct> dbProducts = [];
  num? blindPrice;

  @override
  void onInit() {
    super.onInit();
    dbProducts = [1, 2, 3]
        .map<DBProduct>((item) => DBProduct(
              id: '$item',
              name: 'iphone 13手机-$item',
              note: 'iphone 13手机',
              pic: '',
              price: 3200,
              rate: 0.7,
              nonce: 12345,
            ))
        .toList();
  }

  bool isValidRate() {
    num totalRate = dbProducts.map((item) => item.rate).reduce((x, y) => x + y);
    return totalRate == 1;
  }

  num getBlindPrice() {
    if (blindPrice != null) return blindPrice!;
    num maxPrice = dbProducts.map((item) => item.price).reduce((x, y) => x - y);
    return maxPrice;
  }
}
