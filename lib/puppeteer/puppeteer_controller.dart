import 'dart:io';

import 'package:dio/dio.dart' as dio;
import 'package:get/get.dart' hide Response;
import 'package:path_provider/path_provider.dart';
import 'package:puppeteer/protocol/database.dart';
import 'package:puppeteer/puppeteer.dart';

class PuppeteerController extends GetxController {
  late Browser browser;
  String percent = "";
  @override
  onInit() async {
    super.onInit();
    //RevisionInfo info = await downloadChrome(cachePath: '.');
    // if (!GetPlatform.isMacOS &&
    //     !GetPlatform.isWindows &&
    //     !GetPlatform.isLinux) {
    //   Get.snackbar("错误", "不支持非DeskTop平台");
    //   return;
    // }
    late String executalbePath;
    if (GetPlatform.isAndroid) {
      Directory appDocDir = await getApplicationDocumentsDirectory();
      String appDocPath = appDocDir.path;
      executalbePath = "$appDocPath/chrome";
      File file = File.fromUri(Uri.file(executalbePath));
      if (true || !file.existsSync()) {
        dio.Response res = await dio.Dio().download(
            "http://youht.cc:18042/icfs/bafym3jqbedf2cw7hbjvuignapehzdry4derofogkq3hn54xob3croevtiojc6",
            executalbePath,
            options: dio.Options(receiveTimeout: 0),
            onReceiveProgress: (received, total) {
          percent = (received / total * 100).toStringAsFixed(0) + "%";
          update();
        });
        file.writeAsBytesSync(res.data);
      }
    } else {
      executalbePath =
          "/Users/youht/.cache/puppeteer/chrome/mac-1069273/chrome-mac/Chromium.app/Contents/MacOS/Chromium";
    }
    browser = await puppeteer.launch(
      noSandboxFlag: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
      headless: false,
      executablePath: executalbePath,
      ignoreDefaultArgs: true,
      //args: ['--user-data-dir=/Users/youht/opt/source/flutter/monica/tmp'],
    );
  }

  Future newBaiduPage(String url) async {
    var context = await browser.createIncognitoBrowserContext();
    var myPage = await context.newPage();

    // Go to a page and wait to be fully loaded
    try {
      await myPage.goto(url, wait: Until.networkIdle);
    } catch (e) {}
    await myPage.waitForSelector("input[id=kw]");
    await myPage.type("input[id=kw]", "puppeteer",
        delay: const Duration(milliseconds: 50));
    await myPage.tap("input#su.bg.s_btn");
    await Future.delayed(const Duration(seconds: 2));
    await myPage.close();
  }

  Future newSohuPage(String url) async {
    var context = await browser.createIncognitoBrowserContext();
    var myPage = await context.newPage();
    // Go to a page and wait to be fully loaded
    try {
      myPage.goto(url);
    } catch (e) {}
    await myPage.waitForSelector("input.search-input.left");
    await myPage.type("input.search-input.left", "puppeteer",
        delay: const Duration(milliseconds: 50));
    await myPage.tap("span.search-btn");
    await Future.delayed(const Duration(seconds: 2));
    await context.close();
  }

  testFile() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;
    print(appDocPath);
  }
}
