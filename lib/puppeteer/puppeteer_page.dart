import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'puppeteer_controller.dart';

class PuppeteerPage extends StatelessWidget {
  const PuppeteerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PuppeteerController controller = Get.put(PuppeteerController());
    return Scaffold(
        appBar: AppBar(),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GetBuilder<PuppeteerController>(
                builder: (_) {
                  return Text(controller.percent);
                },
              ),
              GestureDetector(
                  onTap: () async {
                    await controller.testFile();
                  },
                  child: Center(child: Text("file"))),
              GestureDetector(
                  onTap: () async {
                    await controller.newBaiduPage("http://www.baidu.com");
                  },
                  child: Center(child: Text("baidu"))),
              GestureDetector(
                  onTap: () async {
                    await controller.newSohuPage("http://www.sohu.com");
                  },
                  child: Center(child: Text("sohu")))
            ],
          ),
        ));
  }
}
