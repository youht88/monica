import 'package:flutter/material.dart';
import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import 'contacts_controller.dart';

class ContactsPage extends StatelessWidget {
  const ContactsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ContactsController controller = Get.put(ContactsController());
    return Scaffold(
        appBar: AppBar(title: const Text("contacts")),
        body: GetBuilder<ContactsController>(
          builder: (_) {
            return Center(
                child: controller.permissionDenied
                    ? const Center(child: Text('Permission denied'))
                    : controller.contacts == null
                        ? const Center(child: CircularProgressIndicator())
                        : Column(
                            children: [
                              GFButtonBar(children: [
                                GFButton(
                                  text: "提取",
                                  onPressed: () async =>
                                      await controller.pickContract(),
                                ),
                                GFButton(
                                  text: "删除",
                                  onPressed: () async =>
                                      await controller.removeContract(),
                                ),
                                GFButton(
                                  text: "更新",
                                  onPressed: () async =>
                                      await controller.updateContract(),
                                ),
                              ]),
                              Expanded(
                                child: ListView.builder(
                                    itemCount: controller.contacts!.length,
                                    itemBuilder: (context, i) => ListTile(
                                        title: Text(controller
                                            .contacts![i].displayName),
                                        onTap: () async {
                                          final fullContact =
                                              await FlutterContacts.getContact(
                                                  controller.contacts![i].id);
                                          await Get.bottomSheet(
                                            getContactDetail(fullContact),
                                            backgroundColor: Theme.of(context)
                                                .backgroundColor,
                                          );
                                        })),
                              ),
                            ],
                          ));
          },
        ));
  }

  Widget getContactDetail(Contact? fullContact) {
    return SizedBox(
      height: 200,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(children: [
          Text('First name: ${fullContact!.name.first}'),
          Text('Last name: ${fullContact.name.last}'),
          Text(
              'Phone number: ${fullContact.phones.isNotEmpty ? fullContact.phones.first.number : '(none)'}'),
          Text(
              'Email address: ${fullContact.emails.isNotEmpty ? fullContact.emails.first.address : '(none)'}'),
        ]),
      ),
    );
  }
}
