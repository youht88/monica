import 'dart:developer';

import 'package:flutter_contacts/flutter_contacts.dart';
import 'package:get/get.dart';

class ContactsController extends GetxController {
  List<Contact>? contacts;
  Contact? currentContact;
  bool permissionDenied = false;

  @override
  void onInit() async {
    super.onInit();
    fetchContacts();
  }

  Future fetchContacts() async {
    if (!await FlutterContacts.requestPermission(readonly: false)) {
      permissionDenied = true;
      update();
    } else {
      final _contacts = await FlutterContacts.getContacts();
      contacts = _contacts;
      update();
    }
  }

  Future pickContract() async {
    Contact? _currentContact = await FlutterContacts.openExternalPick();
    if (_currentContact == null) return;
    currentContact = await FlutterContacts.getContact(_currentContact.id,
        withAccounts: true);
  }

  Future removeContract() async {
    if (currentContact == null) return;
    await currentContact!.delete();
  }

  Future updateContract() async {
    if (currentContact == null) return;
    currentContact!.name.first = "test";
    await currentContact!.update();
    log(currentContact!.name.first);
  }
}
