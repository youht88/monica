import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:get/get.dart';
import 'package:monica/zwidget/zwidget_controller.dart';

import '../neu/ui/neu_ui.dart';
import '3d_page.dart';

class ZwidgetPage extends StatelessWidget {
  const ZwidgetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ZwidgetController controller = Get.put(ZwidgetController());
    return Scaffold(
        appBar: NeumorphicAppBar(),
        body: GetBuilder<ZwidgetController>(
            builder: (_) => Column(
                  children: [
                    Center(child: _Box()),
                    const SizedBox(height: 50),
                    Center(
                        child: Cao3D(
                      width: 100,
                      height: 100,
                      depth: 100,
                      rotateX: controller.rx1,
                      rotateY: controller.ry1,
                    )),
                    const SizedBox(height: 20),
                    Slider(
                      min: -1.2,
                      max: 1.2,
                      value: controller.rx1,
                      onChanged: (v) {
                        controller.rx1 = v;
                        controller.update();
                      },
                    ),
                    Slider(
                        min: 0.0,
                        max: pi * 10,
                        value: controller.ry1,
                        onChanged: (v) {
                          controller.ry1 = v;
                          controller.update();
                        }),
                  ],
                )));
  }
}

class _Box extends StatelessWidget {
  const _Box({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ZwidgetController controller = Get.put(ZwidgetController());
    return GestureDetector(
        onPanUpdate: (DragUpdateDetails detail) {
          controller.updateOffset(detail);
        },
        onPanEnd: (DragEndDetails detail) {
          controller.resetOffset(detail);
        },
        child: Container(
          child: Transform(
            transform: Matrix4.identity()
              ..setEntry(3, 2, 0.001)
              ..rotateX(controller.ry * pi / 180)
              ..rotateY(controller.rx * pi / 180),
            alignment: Alignment.center,
            child: CubeWidget(
              width: 50,
              height: 50,
              depth: 50,
              front: Center(child: Text("front")),
              back: Center(child: Text("back")),
              right: Center(child: Text("right")),
              left: Center(child: Text("left")),
              top: Center(child: Text("top")),
              bottom: Center(child: Text("bottom")),
            ),
          ),
        ));
  }
}

class CubeWidget extends StatelessWidget {
  double width;
  double? height;
  double? depth;
  Widget? front;
  Widget? back;
  Widget? left;
  Widget? right;
  Widget? top;
  Widget? bottom;
  CubeWidget(
      {Key? key,
      required this.width,
      this.height,
      this.depth,
      this.front,
      this.back,
      this.left,
      this.right,
      this.top,
      this.bottom})
      : super(key: key) {
    height ??= width;
    depth ??= width;
  }
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //front
        Transform(
            transform: Matrix4.translationValues(0.0, 0.0, depth! / -2),
            alignment: Alignment.center,
            child: Container(
                color: Colors.red, height: height, width: width, child: front)),
        //back
        Transform(
            transform: Matrix4.translationValues(0.0, 0.0, depth! / 2),
            alignment: Alignment.center,
            child: Container(
                color: Colors.yellow,
                height: height,
                width: width,
                child: back)),
        //right
        Transform(
            transform: Matrix4.identity()
              ..translate(width / 2, 0.0, 0.0)
              ..rotateY(-pi / 2),
            alignment: Alignment.center,
            child: Container(
                color: Colors.orange,
                height: height,
                width: width,
                child: right)),
        //left
        Transform(
            transform: Matrix4.identity()
              ..translate(width / -2, 0.0, 0.0)
              ..rotateY(-pi / 2),
            alignment: Alignment.center,
            child: Container(
                color: Colors.blue, height: height, width: width, child: left)),
        //top
        Transform(
            transform: Matrix4.identity()
              ..translate(0.0, height! / -2, 0.0)
              ..rotateX(-pi / 2),
            alignment: Alignment.center,
            child: Container(
                color: Colors.green, height: height, width: width, child: top)),
        //bottom
        Transform(
            transform: Matrix4.identity()
              ..translate(0.0, height! / 2, 0.0)
              ..rotateX(-pi / 2),
            alignment: Alignment.center,
            child: Container(
                color: Colors.purple,
                height: height,
                width: width,
                child: bottom)),
      ],
    );
  }
}
