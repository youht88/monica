import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ZwidgetController extends GetxController {
  late double valueX;
  late double valueY;
  double rx = 0;
  double ry = 0;
  double rz = 0;

  double rx1 = 0.5;
  double ry1 = 0.0;
  @override
  onInit() {
    super.onInit();
    updateX(3);
    updateY(4);
  }

  resetOffset(DragEndDetails detail) {
    rx %= pi * 2;
    ry %= pi * 2;
  }

  updateOffset(DragUpdateDetails detail) {
    rx += detail.delta.dx;
    ry += detail.delta.dy;
    // rx %= pi * 2;
    // ry %= pi * 2;
    update();
  }

  updateX(double value) {
    valueX = value;
    update();
  }

  updateY(double value) {
    valueY = value;
    update();
  }
}
