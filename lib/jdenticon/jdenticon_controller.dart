import 'dart:math';

import 'package:get/get.dart';
import 'package:jdenticon_dart/jdenticon_dart.dart';

class JdenticonController extends GetxController {
  late String rawSvg;
  @override
  onInit() async {
    super.onInit();
    rawSvg = Jdenticon.toSvg('123');
  }

  void newId() {
    rawSvg = Jdenticon.toSvg('${Random().nextInt(65535)}');
    update();
  }
}
