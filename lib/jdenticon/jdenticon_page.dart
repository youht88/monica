import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'jdenticon_controller.dart';

class JdenticonPage extends StatelessWidget {
  const JdenticonPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    JdenticonController controller = Get.put(JdenticonController());
    return Scaffold(
        appBar: AppBar(title: const Text("Jdenticon")),
        body: GetBuilder<JdenticonController>(
          builder: (_) {
            return GestureDetector(
              onTap: () {
                controller.newId();
              },
              child: Center(
                  child: SvgPicture.string(
                controller.rawSvg,
                fit: BoxFit.contain,
                height: 128,
                width: 128,
              )),
            );
          },
        ));
  }
}
