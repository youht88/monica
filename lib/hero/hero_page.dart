import 'package:flutter/material.dart';
import 'package:flutter_chip_tags/flutter_chip_tags.dart';
import 'package:get/get.dart';
import 'package:monica/hero/hero_controller.dart';
import 'package:rotated_corner_decoration/rotated_corner_decoration.dart';

import 'custom_rect_tween.dart';
import 'hero_dialog_route.dart';

class HeroPage extends StatelessWidget {
  const HeroPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyHeroController controller = Get.put(MyHeroController());
    return GetBuilder<MyHeroController>(
      init: controller,
      builder: (_) => Scaffold(
          body: Material(
        child: Stack(
          children: [
            Positioned(
                left: 100,
                top: 100,
                child: GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        HeroDialogRoute(
                            builder: (context) => PopupCard("test2", child2)),
                      );
                    },
                    child: Hero(
                        tag: "test2",
                        child: Material(
                            child: Container(
                                width: 50, height: 50, color: Colors.green))))),
            Align(
                alignment: Alignment(0.8, 0.8),
                child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                      HeroDialogRoute(
                          builder: (context) => PopupCard("test1", child1)),
                    );
                  },
                  child: Hero(
                    tag: "test1",
                    // createRectTween: (begin, end) {
                    //   return CustomRectTween(begin: begin!, end: end!);
                    // },
                    child: Material(
                      child: Container(
                          width: 50,
                          height: 50,
                          color: Colors.red,
                          child: Center(child: Text("abcd"))),
                    ),
                  ),
                )),
          ],
        ),
      )),
    );
  }
}

class PopupCard extends StatelessWidget {
  String tag;
  Widget child;
  PopupCard(this.tag, this.child);
  @override
  Widget build(BuildContext context) {
    return Align(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Hero(
          tag: tag,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin!, end: end!);
          },
          child: Material(
            color: Colors.transparent,
            //elevation: 2,
            //shape:
            //    RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
            child: SingleChildScrollView(child: child),
          ),
        ),
      ),
    );
  }
}

Widget child1 = Center(
  child: Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(height: 200, width: 200, color: Colors.red),
  ),
);

Widget child2 = Center(
  child: Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(height: 200, width: 200, color: Colors.green),
  ),
);
