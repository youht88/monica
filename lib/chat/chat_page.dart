import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:markdown_viewer/markdown_viewer.dart';
import 'package:flutter_prism/flutter_prism.dart';
import 'package:monica/chat/chat_controller.dart';
import 'package:monica/neu/ui/neu_ui.dart';

class ChatPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ChatController controller = Get.put(ChatController());
    return GetBuilder<ChatController>(
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
                title: TextField(
                    decoration: InputDecoration(hintText: controller.address),
                    onChanged: (data) {
                      if (data != "") {
                        controller.address = data;
                      }
                    })),
            body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  NeuTextField(
                      maxLines: 4,
                      hintText: "输入指令...",
                      onChanged: (data) {
                        controller.prompt = data;
                      },
                      textEditingController: controller.editController),
                  SizedBox(height: 8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                          onPressed: () {
                            if (controller.parsing) return;
                            controller.prompt = "";
                            controller.completion = "";
                            controller.chatMessageList.flush();
                            controller.editController.clear();
                            controller.update();
                          },
                          child: Text("新话题", style: TextStyle(fontSize: 12))),
                      ElevatedButton(
                          onPressed: () {
                            if (controller.parsing) return;
                            controller.prompt = "";
                            controller.editController.clear();
                            //controller.update();
                          },
                          child: Text("清空指令", style: TextStyle(fontSize: 12))),
                      ElevatedButton(
                          onPressed: () async {
                            if (controller.parsing) return;
                            controller.parsing = true;
                            controller.update();
                            await controller.sendMessage();
                            controller.parsing = false;
                            controller.update();
                          },
                          child: Text("${controller.parsing ? '稍等...' : '发送'}",
                              style: TextStyle(fontSize: 12))),
                      ElevatedButton(
                          onPressed: () async {
                            if (controller.parsing) return;
                            await Clipboard.setData(ClipboardData(
                                text:
                                    "${controller.prompt}\n${controller.completion}"));
                          },
                          child: Text("复制", style: TextStyle(fontSize: 12))),
                    ],
                  ),
                  SizedBox(height: 8),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SingleChildScrollView(
                        child: Align(
                      alignment: Alignment.centerLeft,
                      child: MarkdownViewer(
                        controller.completion,
                        syntaxExtensions: [ExampleSyntax()],
                        highlightBuilder: (text, language, infoString) {
                          final prism = Prism(
                            mouseCursor: SystemMouseCursors.text,
                            style:
                                Theme.of(context).brightness == Brightness.dark
                                    ? const PrismStyle.dark()
                                    : const PrismStyle(),
                          );
                          return prism.render(text, language ?? 'plain');
                        },
                      ),
                    )),
                  )),
                ],
              ),
            ));
      },
    );
  }
}

class ExampleSyntax extends MdInlineSyntax {
  ExampleSyntax() : super(RegExp(r'#[^#]+?(?=\s+|$)'));

  @override
  MdInlineObject? parse(MdInlineParser parser, Match match) {
    final markers = [parser.consume()];
    final content = parser.consumeBy(match[0]!.length - 1);

    // return MdInlineElement(
    //   'example',
    //   markers: markers,
    //   children: content.map((e) => MdText.fromSpan(e)).toList(),

    // );
  }
}
