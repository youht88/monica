import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:xterm/xterm.dart';

import 'package:socket_io_client/socket_io_client.dart' as IO;

class XtermController extends GetxController {
  late Terminal terminal;

  late WebSocketChannel channel;
  @override
  onInit() async {
    super.onInit();
    // TerminalStyle.fromTextStyle(
    //     TextStyle(fontFamily: "Roboto", fontSize: 8, color: Colors.red));
    terminal = Terminal();
    channel = WebSocketChannel.connect(Uri.parse("ws://youht.cc:7777"));
    channel.stream.listen((message) {
      terminal.write(message);
    });
    terminal.buffer.clear();
    terminal.buffer.setCursor(0, 0);
    terminal.onOutput = (data) {
      channel.sink.add(data);
      //session.write(utf8.encode(data) as Uint8List);
    };
    // IO.Socket socket = IO.io(
    //     'ws://youht.cc:7778',
    //     OptionBuilder().setTransports(['websocket']) // for Flutter or Dart VM
    //         .build());
    // socket.onConnect((_) {
    //   Get.snackbar('connect', "ok");
    //   //socket.emit('msg', 'test');
    // });
  }

  @override
  onClose() {
    channel.sink.add("exit");
    super.onClose();
  }

  test() {
    String command = "docker run -it node\r\n";
    channel.sink.add(command);
  }
}
