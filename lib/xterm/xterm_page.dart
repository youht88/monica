import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:get/get.dart';
import 'package:monica/neu/ui/neu_ui.dart';
import 'package:xterm/xterm.dart';

import 'xterm_controller.dart';

class XtermPage extends StatelessWidget {
  const XtermPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    XtermController controller = Get.put(XtermController());
    return Scaffold(
        appBar: AppBar(),
        body: GetBuilder<XtermController>(
          builder: (_) => Container(
            width: Get.width,
            height: Get.height,
            child: Column(
              children: [
                NeuButton("abc", onPressed: () {
                  controller.test();
                }),
                Expanded(
                    child: NeuContainer(
                  depth: -1,
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: [
                      const Expanded(
                          flex: 1,
                          child: Markdown(
                              selectable: true,
                              data:
                                  "# abc\n##  xyz\n* hello world,I am you hai tao\n* nice to meet you")),
                      Expanded(
                        flex: 4,
                        child: TerminalView(controller.terminal,
                            padding: EdgeInsets.all(10)),
                      ),
                    ],
                  ),
                )),
              ],
            ),
          ),
        ));
  }
}
