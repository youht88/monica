import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../neu/ui/neu_ui.dart';
import 'ws_server_controller.dart';

class WsServerPage extends StatelessWidget {
  const WsServerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WsServerController controller = Get.put(WsServerController());
    return Scaffold(
        appBar: AppBar(title: Center(child: Text("web socket server"))),
        body: GetBuilder<WsServerController>(
          builder: (_) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                ExpansionPanelList(
                    expansionCallback: (_, show) {
                      controller.config = !show;
                      controller.update();
                    },
                    children: [
                      ExpansionPanel(
                          isExpanded: controller.config,
                          headerBuilder: (context, b) {
                            return Center(
                                child: NeuText("配置", weight: FontWeight.bold));
                          },
                          body: Column(children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: NeuTextField(
                                        hintText: "host",
                                        onChanged: (value) {},
                                        textEditingController:
                                            controller.hostController),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: NeuTextField(
                                        hintText: "port",
                                        onChanged: (value) {},
                                        textEditingController:
                                            controller.portController),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              //mainAxisSize: MainAxisSize.min,
                              children: [
                                Expanded(
                                  //width: 200,
                                  child: NeuTextField(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      hintText: "username",
                                      onChanged: (value) {},
                                      textEditingController:
                                          controller.usernameController),
                                ),
                                Expanded(
                                  //width: 200,
                                  child: NeuTextField(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      hintText: "password",
                                      obscureText: true,
                                      onChanged: (value) {},
                                      textEditingController:
                                          controller.passwordController),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: NeuButton("启动服务", onPressed: () async {
                                    await controller.startWebSocketServer();
                                  }),
                                ),
                              ],
                            )
                          ]))
                    ]),
                Expanded(
                  child: ListView.builder(
                      itemCount: controller.logs.length,
                      itemBuilder: (_, idx) {
                        return Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(controller.logs[idx]),
                        );
                      }),
                ),
              ],
            ),
          ),
        ));
  }
}
