import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dartssh2/dartssh2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shelf/src/handler.dart';
import 'package:shelf_web_socket/shelf_web_socket.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:socket_io/socket_io.dart';

class WsServerController extends GetxController {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController hostController = TextEditingController();
  TextEditingController portController = TextEditingController();
  String get host => hostController.text;
  int get port => int.parse(portController.text);
  String get username => usernameController.text;
  String get password => passwordController.text;
  bool config = true;

  List<String> logs = [];

  late HttpServer webSocketServer;
  late Server ioSocketServer;
  String mode = 'webSocket'; //ioSocket || webSocket
  @override
  onInit() {
    super.onInit();
    hostController.text = "localhost";
    portController.text = "22";
    usernameController.text = "youht";
    passwordController.text = "";
  }

  void addLog(String logMsg) {
    logs.add("${DateTime.now()} $logMsg");
    update();
  }

  Future<void> startWebSocketServer() async {
    var handler = webSocketHandler((webSocket) async {
      final session = await initSSH();
      if (session == null) return;
      webSocket.stream.listen((message) {
        session.write(Uint8List.fromList(utf8.encode(message)));
      });
      session.stdout
          .cast<List<int>>()
          .transform(const Utf8Decoder())
          .listen(webSocket.sink.add);
      session.stderr
          .cast<List<int>>()
          .transform(const Utf8Decoder())
          .listen(webSocket.sink.add);
    });
    webSocketServer = await shelf_io.serve(handler, '0.0.0.0', 12345);
    addLog(
        'Serving at ws://${webSocketServer.address.host}:${webSocketServer.port}');
  }

  Future<void> startIOSocketServer() async {
    //尚不正确
    ioSocketServer = Server();
    //var nsp = server.of('/myNameSpace');
    ioSocketServer.on('connection', (webSocket) async {
      final SSHSession? session = await initSSH();
      if (session == null) return;
      webSocket.on("message", (message) {
        session.write(Uint8List.fromList(utf8.encode(message)));
      });
      session.stdout
          .cast<List<int>>()
          .transform(const Utf8Decoder())
          .listen(webSocket.sink.add);
      session.stderr
          .cast<List<int>>()
          .transform(const Utf8Decoder())
          .listen(webSocket.sink.add);
    });
    await ioSocketServer.listen(12345);
    addLog("IOServer服务已启动在0.0.0.0:12345 !!");
  }

  dynamic initSSH() async {
    try {
      var socket = await SSHSocket.connect(host, port);
      SSHClient client = SSHClient(
        socket,
        username: username,
        onPasswordRequest: () => password,
      );
      await client.authenticated;

      //SSHSession session = await client.execute("/usr/local/bin/docker run -it node");
      SSHSession session = await client.shell();
      addLog("登陆成功!");
      return session;
    } catch (e) {
      addLog("登陆失败:$e");
    }
  }

  @override
  onClose() {
    if (mode == "webSocket") {
      webSocketServer.close();
    } else {
      ioSocketServer.close();
    }
    super.onClose();
  }
}
