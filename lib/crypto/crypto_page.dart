import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import '../neu/neu.dart';
import 'crypto_controller.dart';

class CryptoPage extends StatelessWidget {
  const CryptoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CryptoController controller = Get.put(CryptoController());
    return Scaffold(
        appBar: AppBar(),
        body: GetBuilder<CryptoController>(
          builder: (_) {
            return SingleChildScrollView(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          NeuText("私钥:"),
                          const SizedBox(height: 8),
                          NeuTextField(
                              minLines: 2,
                              maxLines: 2,
                              textEditingController:
                                  controller.privateKeyController,
                              onChanged: (value) {
                                controller.privateKey = value;
                              })
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          NeuText("公钥:"),
                          const SizedBox(height: 8),
                          NeuTextField(
                              minLines: 4,
                              maxLines: 4,
                              textEditingController:
                                  controller.publicKeyController,
                              onChanged: (value) {
                                controller.publicKey = value;
                              })
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(children: [
                        NeuButton(
                          "生成密钥对",
                          onPressed: () {
                            controller.genKeypair();
                          },
                        ),
                        SizedBox(width: 8),
                        NeuButton("用私钥生成公钥", onPressed: () {
                          controller.genPublicKey();
                        }),
                        const SizedBox(width: 20),
                        NeuButton("清空密钥", onPressed: () {
                          controller.clearKeypair();
                        }),
                      ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              NeuText("数据:"),
                              const SizedBox(width: 20),
                              NeuSwitch(
                                  value: controller.isHex,
                                  onChanged: (value) {
                                    controller.changeDataMode(value);
                                  }),
                              const SizedBox(width: 8),
                              NeuText("${controller.dataMode}"),
                            ],
                          ),
                          const SizedBox(height: 8),
                          NeuTextField(
                              minLines: 8,
                              maxLines: 8,
                              textEditingController:
                                  controller.messageController,
                              onChanged: (value) {
                                controller.msg = value;
                                //controller.update();
                              })
                        ],
                      ),
                    ),
                    GFAccordion(
                      collapsedTitleBackgroundColor:
                          NeumorphicTheme.of(context)!.current!.baseColor,
                      titleChild: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(children: [
                          NeuButton(
                            "公钥加密",
                            onPressed: () {
                              controller.encrypt();
                            },
                          ),
                          const SizedBox(width: 8),
                          NeuButton("私钥解密", onPressed: () {
                            controller.decrypt();
                          }),
                          const SizedBox(width: 20),
                          Column(
                            children: [
                              NeuSwitch(
                                  value: controller.isStand,
                                  onChanged: (value) {
                                    controller.changeCipherMode(value);
                                  }),
                              const SizedBox(height: 8),
                              NeuText("${controller.cipherMode}"),
                            ],
                          ),
                        ]),
                      ),
                      contentChild: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          NeuText("加/解密结果:"),
                          const SizedBox(height: 8),
                          NeuTextField(
                              //minLines: 8,
                              maxLines: 8,
                              textEditingController:
                                  controller.cipherController,
                              onChanged: (value) {})
                        ],
                      ),
                    ),
                    GFAccordion(
                      collapsedTitleBackgroundColor:
                          NeumorphicTheme.of(context)!.current!.baseColor,
                      titleChild: Row(children: [
                        NeuButton(
                          "私钥签名",
                          onPressed: () {
                            controller.sign();
                          },
                        ),
                        SizedBox(width: 8),
                        NeuButton("公钥验签", onPressed: () {
                          controller.verify();
                        }),
                      ]),
                      contentChild: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            NeuText("签名结果:"),
                            const SizedBox(height: 8),
                            NeuTextField(
                                maxLines: 8,
                                textEditingController:
                                    controller.signController,
                                onChanged: (value) {
                                  controller.signResult = value;
                                }),
                            const SizedBox(width: 8),
                            Row(
                              children: [
                                NeuText("验签结果:"),
                                const SizedBox(width: 8),
                                NeuText(
                                    "${controller.verifyResult ? '验证通过' : '验证失败'}")
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
            ));
          },
        ));
  }
}
