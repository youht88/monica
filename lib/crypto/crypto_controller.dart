import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:monica/util/crypto_util.dart';

class CryptoController extends GetxController {
  late String privateKey;
  late String publicKey;
  String msg = "马到成功";
  late String signResult;
  late bool verifyResult = false;
  late String encrypto;
  late String decrypto;
  late String cipherResult;
  bool isHex = true;
  String dataMode = 'HEX';
  bool isStand = false;
  String cipherMode = 'C1C2C3';
  TextEditingController privateKeyController = TextEditingController();
  TextEditingController publicKeyController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  TextEditingController cipherController = TextEditingController();
  TextEditingController signController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    final key = CryptoLib.genSM2KeyPair();
    privateKey = key['privateKey'];
    publicKey = key['publicKey'];
    signResult = CryptoLib.sign(msg, privateKey);
    verifyResult = CryptoLib.verify(msg, publicKey, signResult);
    encrypto = CryptoLib.encrypt(msg, publicKey, cipherMode: 0);
    decrypto = "${CryptoLib.decrypt(encrypto, privateKey, cipherMode: 0)}";
    decrypto = CryptoLib.decrypt(
            '0428e70a421804fb3569f29c5c3ca0ad72b2b72ca163de253269e509a7ce97f06b44dbfd15b5c6224194382f1953a2594482095d031b3974de2af86292357bfb099a82048012fbb013c6668e4a22508bb401919085f8e85329632ed08193adfb9b6a6b321e0c3bfa10e90912',
            '5d51d0478e70dd6711dbb93c1ddfecd4b4ef54bd1a65fc2e957d195c44c54b1c',
            cipherMode: 0) ??
        'error';
    update();
  }

  void genKeypair() {
    final keypair = CryptoLib.genSM2KeyPair();
    privateKey = keypair['privateKey'];
    publicKey = keypair['publicKey'];
    privateKeyController.text = privateKey;
    publicKeyController.text = publicKey;
    update();
  }

  void genPublicKey() {
    publicKey = CryptoLib.getPublicKeyByPrivateKey(privateKey,
        algType: CryptoAlgorithm.SM2);
    publicKeyController.text = publicKey;
    update();
  }

  void clearKeypair() {
    publicKey = "";
    privateKey = "";
    privateKeyController.text = privateKey;
    publicKeyController.text = publicKey;
    update();
  }

  void changeDataMode(value) {
    isHex = value;
    dataMode = isHex ? 'HEX' : 'ASCII';
    update();
  }

  void changeCipherMode(value) {
    isStand = value;
    cipherMode = isStand ? 'C1C3C2' : 'C1C2C3';
    update();
  }

  void encrypt() {
    String encryptMsg;
    dynamic toMsg;
    if (dataMode == "HEX") {
      toMsg = BufferLib.hexStringToBuffer(msg);
    } else {
      toMsg = msg;
    }
    if (cipherMode == "C1C3C2") {
      encryptMsg = CryptoLib.encrypt(toMsg, publicKey, cipherMode: 1);
    } else {
      encryptMsg = CryptoLib.encrypt(toMsg, publicKey, cipherMode: 0);
    }
    cipherController.text = encryptMsg;
    cipherResult = encryptMsg;
    update();
  }

  void decrypt() {
    String? encryptMsg;
    dynamic toMsg;
    if (dataMode == "HEX") {
      toMsg = msg;
    } else {
      toMsg = msg;
    }
    if (cipherMode == "C1C3C2") {
      encryptMsg = CryptoLib.decrypt(toMsg, privateKey, cipherMode: 1);
    } else {
      encryptMsg = CryptoLib.decrypt(toMsg, privateKey, cipherMode: 0);
    }
    cipherResult = encryptMsg ?? '[error]';
    cipherController.text = cipherResult;

    update();
  }

  void sign() {
    dynamic toMsg;
    if (dataMode == "HEX") {
      toMsg = BufferLib.hexStringToBuffer(msg);
    } else {
      toMsg = msg;
    }

    signResult = CryptoLib.sign(toMsg, privateKey,
        algType: CryptoAlgorithm.SM2, hashType: HashAlgorithm.SM3);
    signController.text = signResult;
    update();
  }

  void verify() {
    dynamic toMsg;
    verifyResult = false;
    update();
    if (dataMode == "HEX") {
      toMsg = BufferLib.hexStringToBuffer(msg);
    } else {
      toMsg = msg;
    }

    verifyResult = CryptoLib.verify(toMsg, publicKey, signResult,
        algType: CryptoAlgorithm.SM2, hashType: HashAlgorithm.SM3);
    update();
  }
}
