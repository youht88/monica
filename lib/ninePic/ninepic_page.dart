import 'package:flutter/material.dart';
import 'package:nine_grid_view/nine_grid_view.dart';

class NinepicPage extends StatelessWidget {
  const NinepicPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Nine Pic")),
        body: Center(
          child: NineGridView(
              width: 150,
              height: 150,
              arcAngle: 10,
              type: NineGridType.weChatGp,
              itemCount: 7,
              itemBuilder: (context, idx) {
                return Container(color: Colors.pink);
              }),
        ),
        bottomSheet: Container(height: 20));
  }
}
