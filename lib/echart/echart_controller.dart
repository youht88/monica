import 'package:get/get.dart';

class EchartController extends GetxController {
  List dataObj = [];
  @override
  onInit() async {
    super.onInit();
    await getData1();
  }

  getData1() async {
    await Future.delayed(const Duration(seconds: 4));

    dataObj = [
      {
        'name': 'Jan',
        'value': 8726.2453,
      },
      {
        'name': 'Feb',
        'value': 2445.2453,
      },
      {
        'name': 'Mar',
        'value': 6636.2400,
      },
      {
        'name': 'Apr',
        'value': 4774.2453,
      },
      {
        'name': 'May',
        'value': 1066.2453,
      },
      {
        'name': 'Jun',
        'value': 4576.9932,
      },
      {
        'name': 'Jul',
        'value': 8926.9823,
      }
    ];
    update();
  }
}
