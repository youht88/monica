// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:monica/neu/neu.dart';

// import 'alarm_controller.dart';

// class AlarmPage extends StatelessWidget {
//   const AlarmPage({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     AlarmController controller = Get.put(AlarmController());
//     return Scaffold(
//         appBar: AppBar(),
//         body: Column(children: [
//           NeuButton(
//             "增加",
//             onPressed: () async {
//               controller.add();
//             },
//           ),
//           NeuButton(
//             "修改",
//             onPressed: () async {
//               controller.modify(controller.eventId);
//             },
//           ),
//           NeuButton(
//             "删除",
//             onPressed: () async {
//               controller.delete(controller.eventId);
//             },
//           ),
//         ]));
//   }
// }
