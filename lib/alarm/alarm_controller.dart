// import 'package:alarm_calendar/alarm_calendar_plugin.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// class AlarmController extends GetxController {
//   String eventId = "";
//   @override
//   onInit() {
//     super.onInit();
//   }

//   add() async {
//     Calendars calendars = Calendars(
//         DateTime.now(),
//         DateTime.now().add(Duration(minutes: 15)),
//         '测试通知',
//         '测试通知描述',
//         [5],
//         '1',
//         0);
//     //查询是否有读权限。
//     await AlarmCalendar.CheckReadPermission().then((res) async {
//       if (res != null) {
//         //查询是否有写权限
//         await AlarmCalendar.CheckWritePermission().then((resWrite) async {
//           if (resWrite != null) {
//             final id = await AlarmCalendar.createEvent(calendars);
//             calendars.setEventId = id!;
//             eventId = id;
//             Get.snackbar('获得ID', id);
//           }
//         });
//       }
//     });
//   }

//   modify(String eventId) async {
//     Calendars? calendars = await select(eventId);
//     if (calendars == null) return;
//     calendars.setTitle = '测试通知修改版';
//     calendars.setAlert = [3, 15];
//     calendars.setStartTime = DateTime.now();
//     calendars.setEndTime = DateTime.now().add(Duration(minutes: 30));
//     calendars.setAllDay = 0;
//     calendars.setNote = '这里是备注内容';
//     final id = await AlarmCalendar.updateEvent(calendars);
//     Get.snackbar("修改日程ID", id!);
//     calendars.setEventId = id;
//     eventId = id;
//   }

//   delete(String id) async {
//     final status = await AlarmCalendar.deleteEvent(id);
//     Get.snackbar("删除状态：$status", id);
//   }

//   Future<Calendars?> select(String id) async {
//     //查询是否有读权限。
//     Calendars? calendars;
//     bool? res = await AlarmCalendar.CheckReadPermission();
//     if (res != null) {
//       //查询是否有写权限
//       bool? resWrite = await AlarmCalendar.CheckWritePermission();
//       if (resWrite != null) {
//         final result = (await AlarmCalendar.selectEvent(id))['data'];
//         debugPrint("$result");
//         Get.snackbar('获取返回数据', "$result");
//         calendars = Calendars(
//             DateTime.fromMillisecondsSinceEpoch(result['startTime']),
//             DateTime.fromMillisecondsSinceEpoch(result['endTime']),
//             result['title'],
//             result['notes'],
//             result['alert'],
//             "${result['id']}",
//             result['allDay']);
//         return calendars;
//       }
//     }
//   }
// }
