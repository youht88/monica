import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

import 'getwidget_controller.dart';

class GetWidgetPage extends StatelessWidget {
  GetWidgetPage({Key? key}) : super(key: key);
  final GetWidgetController controller = Get.put(GetWidgetController());

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, designSize: const Size(360, 690));
    return Scaffold(
        appBar: AppBar(
            leading: GestureDetector(
                onTap: () => Get.back(closeOverlays: true),
                child: const Icon(Icons.backspace))),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: GetBuilder<GetWidgetController>(
              builder: (_) {
                return Column(
                  children: !controller.isReady
                      ? List.generate(10, (int index) => const _ShimmerWidget())
                      : List.generate(
                          10,
                          (int index) => GestureDetector(
                                onTap: () => Get.to(const _BottomWidget()),
                                child: const GFListTile(
                                    avatar: GFAvatar(),
                                    title: Text('Title',
                                        style: TextStyle(color: Colors.blue)),
                                    subTitle: Text(
                                        'Lorem ipsum dolor sit amet, consectetur adipiscing'),
                                    icon: Icon(Icons.favorite)),
                              )),
                );
              },
            ),
          ),
        ));
  }
}

class _ShimmerWidget extends StatelessWidget {
  const _ShimmerWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GFShimmer(
        child: Row(children: [
          Container(
            width: 60,
            height: 60.h,
            color: Colors.white,
          ),
          SizedBox(width: 4.w),
          SizedBox(
            height: 60.h,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(height: 20.h, width: 60.w, color: Colors.white),
                Container(height: 12.h, width: 120.w, color: Colors.white),
                Container(height: 12.h, width: 150.w, color: Colors.white),
              ],
            ),
          )
        ]),
      ),
    );
  }
}

class _BottomWidget extends StatelessWidget {
  const _BottomWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GetWidgetController controller = Get.find();
    final _controller = controller.bottomController;
    return Scaffold(
      bottomSheet: GFBottomSheet(
        controller: _controller,
        maxContentHeight: 150.h,
        stickyHeaderHeight: 100.h,
        stickyHeader: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [BoxShadow(color: Colors.black45, blurRadius: 0)]),
          child: const GFListTile(
            avatar: GFAvatar(
                //backgroundImage: AssetImage('assets image here'),
                ),
            titleText: 'GetWidget',
            subTitleText: 'Open source UI library',
          ),
        ),
        contentBody: Container(
          height: 200.h,
          margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.h),
          child: ListView(
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            children: const [
              Center(
                  child: Text(
                'Getwidget reduces your overall app development time to minimum 30% because of its pre-build clean UI widget that you can use in flutter app development. We have spent more than 1000+ hours to build this library to make flutter developer’s life easy.',
                style: TextStyle(
                    fontSize: 15, wordSpacing: 0.3, letterSpacing: 0.2),
              ))
            ],
          ),
        ),
        stickyFooter: Container(
          color: GFColors.SUCCESS,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              Text(
                'Get in touch',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
              Text(
                'info@getwidget.dev',
                style: TextStyle(fontSize: 15, color: Colors.white),
              ),
            ],
          ),
        ),
        stickyFooterHeight: 50.h,
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: GFColors.SUCCESS,
          child: _controller.isBottomSheetOpened
              ? const Icon(Icons.keyboard_arrow_down)
              : const Icon(Icons.keyboard_arrow_up),
          onPressed: () {
            _controller.isBottomSheetOpened
                ? _controller.hideBottomSheet()
                : _controller.showBottomSheet();
          }),
    );
  }
}
