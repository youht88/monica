import 'dart:async';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:getwidget/getwidget.dart';

class GetWidgetController extends GetxController {
  bool isReady = false;
  final GFBottomSheetController bottomController = GFBottomSheetController();
  @override
  void onInit() {
    super.onInit();
    log("abc");
    Timer(const Duration(seconds: 3), () {
      log("$isReady");
      isReady = true;
      update();
    });
  }
}
