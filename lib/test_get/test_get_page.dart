part of test_get;

class TestGetPage extends StatelessWidget {
  const TestGetPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TestGetController controller = Get.put(TestGetController());
    return Scaffold(
        appBar: NeumorphicAppBar(title: NeuText("Get Test")),
        backgroundColor: NeumorphicTheme.baseColor(context),
        body: SizedBox(
          child: SizedBox(
            height: Get.height,
            child: Center(
              child: Column(children: [
                Padding(
                    padding: EdgeInsets.all(8.0), child: _Get1Page(controller)),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: GetBuilder(
                    init: controller.get2Controller,
                    builder: (_) {
                      return _Get2Page(controller);
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: _Get3Page(controller, controller.get31Controller),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: _Get3Page(controller, controller.get32Controller),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GetBuilder<TestGetController>(
                    builder: (_) {
                      return Text("${controller.count}");
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ElevatedButton(
                      onPressed: () {
                        controller.reset();
                      },
                      child: Text("reset")),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Get4Page(),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: APage(Get.find<AController>(tag: "B")),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: APage(Get.find<AController>(tag: "B")),
                )
              ]),
            ),
          ),
        ));
  }
}

class APage extends StatelessWidget {
  late AController controller;
  APage(this.controller, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AController>(
      init: controller,
      global: false,
      builder: (_) {
        return GestureDetector(
            onTap: () {
              controller.msg = "xyz";
              controller.update();
            },
            child: Container(child: Center(child: Text(controller.msg))));
      },
    );
  }
}
