part of test_get;

class AController extends GetxController {
  String msg = "abc";
  @override
  onInit() {
    super.onInit();
    debugPrint("A onInit");
  }
}

class TestGetController extends GetxController {
  late _Get1Controller get1Controller;
  late _Get2Controller get2Controller;
  late _Get3Controller get31Controller;
  late _Get3Controller get32Controller;
  late Get4Controller get4Controller;
  late AController aController;
  int count = 0;
  @override
  onInit() {
    super.onInit();
    get1Controller = Get.put(_Get1Controller(this));
    get2Controller = Get.put(_Get2Controller(this));
    Get.create(() => _Get3Controller(this), permanent: false);
    get31Controller = Get.find<_Get3Controller>();
    get31Controller.id = "get31";
    get31Controller.text = "get31";
    get31Controller.color = Colors.yellow;
    get32Controller = Get.find<_Get3Controller>();
    get32Controller.id = "get32";
    get32Controller.text = "get32";
    get32Controller.color = Colors.purple;
    get4Controller = Get.put(Get4Controller());

    Get.lazyPut(() => AController(), tag: "A");
    Get.create(() => AController(), tag: "B");
    AController bController = Get.find<AController>(tag: "A");
    AController cController = Get.find<AController>(tag: "A");
    AController dController = Get.find<AController>(tag: "B");
    AController eController = Get.find<AController>(tag: "B");

    assert(bController == cController);
    assert(dController != eController);
  }

  add() {
    count++;
    update();
  }

  reset() {
    get1Controller.changeFromOther("get1");
    get2Controller.changeFromOther("get2");
    get31Controller.changeText("get31");
    get32Controller.changeText("get32");
    get4Controller.changeShow(false);
    get4Controller.get41Controller.changeColor(Colors.yellow);
    get4Controller.get42Controller.changeColor(Colors.purple);
    count = 0;
    update();
  }
}
