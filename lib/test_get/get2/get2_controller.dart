part of test_get;

class _Get2Controller extends GetxController {
  late TestGetController testGetController;
  _Get2Controller(this.testGetController);
  String text = "get2";
  changeText(String msg) {
    text = msg;
    testGetController.add();
    update();
    testGetController.get1Controller.changeFromOther("get2->get1");
    testGetController.get32Controller.changeFromOther("get2->get32");
    testGetController.get4Controller.changeShow(true);
    testGetController.get4Controller.get42Controller.changeColor(Colors.green);
  }

  changeFromOther(String msg) {
    text = msg;
    update();
  }
}
