part of test_get;

class _Get2Page extends StatelessWidget {
  late TestGetController testGetController;
  _Get2Page(this.testGetController, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _Get2Controller controller = Get.put(_Get2Controller(testGetController));
    return GestureDetector(
        onTap: () {
          controller.changeText("GET2");
        },
        child: NeuContainer(
            width: 100,
            height: 100,
            depth: 5,
            shape: NeumorphicShape.concave,
            color: Colors.green,
            child: Center(child: Text(controller.text))));
  }
}
