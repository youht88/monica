part of test_get;

class _Get3Page extends StatelessWidget {
  late TestGetController testGetController;
  late _Get3Controller controller;
  _Get3Page(this.testGetController, this.controller, {Key? key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          controller.changeText("GET3");
        },
        child: Center(
          child: Container(
              height: 100,
              width: 100,
              color: controller.color,
              child: Center(
                  child: GetBuilder<_Get3Controller>(
                global: false,
                init: controller,
                builder: (_) {
                  return Text(controller.text);
                },
              ))),
        ));
  }
}
