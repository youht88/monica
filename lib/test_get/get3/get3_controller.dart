part of test_get;

class _Get3Controller extends GetxController {
  late TestGetController testGetController;
  _Get3Controller(this.testGetController);
  String id = "";
  String text = "";
  Color? color;
  changeText(String msg) {
    text = msg;
    testGetController.add();
    update();
    switch (id) {
      case "get31":
        testGetController.get4Controller.get41Controller
            .changeColor(Colors.yellow);
        break;
      case "get32":
        testGetController.get4Controller.get42Controller
            .changeColor(Colors.purple);
        break;
    }
  }

  changeFromOther(String msg) {
    text = msg;
    update();
  }
}
