library test_get;

import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:monica/neu/ui/neu_ui.dart';
import 'package:monica/test_get/get4/get4.dart';

part 'test_get_controller.dart';
part 'test_get_page.dart';
part 'get1/get1_controller.dart';
part 'get1/get1_page.dart';
part 'get2/get2_controller.dart';
part 'get2/get2_page.dart';
part 'get3/get3_controller.dart';
part 'get3/get3_page.dart';
