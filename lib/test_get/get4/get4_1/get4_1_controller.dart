part of get4;

class _Get4_1_Controller extends GetxController {
  Color _color = Colors.yellow;
  get color => _color;
  changeColor(Color color) {
    _color = color;
    update();
  }
}
