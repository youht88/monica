part of get4;

class _Get4_1_Page extends StatelessWidget {
  const _Get4_1_Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _Get4_1_Controller controller = Get.put(_Get4_1_Controller());
    return GetBuilder<_Get4_1_Controller>(
      init: controller,
      builder: (_) {
        return Container(height: 50, width: 50, color: controller.color);
      },
    );
  }
}
