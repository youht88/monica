part of get4;

class Get4Page extends StatelessWidget {
  const Get4Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get4Controller controller = Get.put(Get4Controller());
    return GetBuilder<Get4Controller>(
      builder: (_) => controller.show
          ? Container(
              height: 100,
              width: 100,
              child: Stack(children: [
                Align(alignment: Alignment.topLeft, child: _Get4_1_Page()),
                Align(alignment: Alignment.bottomRight, child: _Get4_2_Page()),
              ]),
            )
          : Container(),
    );
  }
}
