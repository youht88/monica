part of get4;

class Get4Controller extends GetxController {
  late _Get4_1_Controller get41Controller;
  late _Get4_2_Controller get42Controller;
  bool show = false;
  @override
  onInit() {
    super.onInit();
    get41Controller = Get.put(_Get4_1_Controller());
    get42Controller = Get.put(_Get4_2_Controller());
  }

  changeShow(bool _show) {
    show = _show;
    update();
  }
}
