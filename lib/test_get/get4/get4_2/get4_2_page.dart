part of get4;

class _Get4_2_Page extends StatelessWidget {
  const _Get4_2_Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _Get4_2_Controller controller = Get.put(_Get4_2_Controller());
    return GetBuilder<_Get4_2_Controller>(
      init: controller,
      builder: (_) {
        return Container(height: 50, width: 50, color: controller.color);
      },
    );
  }
}
