part of get4;

class _Get4_2_Controller extends GetxController {
  Color _color = Colors.purple;
  get color => _color;
  changeColor(Color color) {
    _color = color;
    update();
  }
}
