library get4;

import 'package:flutter/material.dart';
import 'package:get/get.dart';

part './get4_1/get4_1_controller.dart';
part './get4_1/get4_1_page.dart';
part './get4_2/get4_2_controller.dart';
part './get4_2/get4_2_page.dart';

part './get4_controller.dart';
part './get4_page.dart';
