part of test_get;

class _Get1Controller extends GetxController {
  late TestGetController testGetController;
  _Get1Controller(this.testGetController);
  String text = "get1";
  changeText(String msg) {
    text = msg;
    testGetController.add();
    update();
    testGetController.get2Controller.changeFromOther("get1->get2");
    testGetController.get31Controller.changeFromOther("get1->get31");
    testGetController.get4Controller.changeShow(true);
    testGetController.get4Controller.get41Controller.changeColor(Colors.red);
  }

  changeFromOther(String msg) {
    text = msg;
    update();
  }
}
