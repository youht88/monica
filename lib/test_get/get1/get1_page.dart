part of test_get;

class _Get1Page extends StatelessWidget {
  TestGetController testGetController;
  _Get1Page(this.testGetController, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _Get1Controller controller = Get.put(_Get1Controller(testGetController));
    return GestureDetector(
        onTap: () {
          controller.changeText("GET1");
        },
        child: GetBuilder(
          init: testGetController,
          builder: (_) => GetBuilder<_Get1Controller>(
            builder: (_) {
              return NeuContainer(
                  width: 100,
                  height: 100,
                  depth: 5,
                  shape: NeumorphicShape.convex,
                  color: Colors.red,
                  child: Center(
                      child: Text(
                          "${controller.text},${testGetController.count}")));
            },
          ),
        ));
  }
}
