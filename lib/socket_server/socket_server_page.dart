import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:get/get.dart';

import '../neu/ui/neu_ui.dart';
import 'socket_server_controller.dart';

class SocketServerPage extends StatelessWidget {
  const SocketServerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SocketServerController controller = Get.put(SocketServerController());
    Map<LogType, Widget> leadingIconMap = {
      LogType.info: const Icon(Icons.info, color: Colors.blue),
      LogType.error: const Icon(Icons.error, color: Colors.red),
      LogType.success: const Icon(Icons.ac_unit, color: Colors.green),
      LogType.warning: const Icon(Icons.warning, color: Colors.yellow)
    };
    return Scaffold(
        appBar: NeumorphicAppBar(title: NeuText("socket server 日志")),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(children: [
            Expanded(
                child: NeuContainer(child: GetBuilder<SocketServerController>(
              builder: (_) {
                return ListView.builder(
                    reverse: false,
                    controller: controller.scrollController,
                    itemCount: controller.logs.length,
                    itemBuilder: (_, idx) => NeuTile(
                        leading: leadingIconMap[controller.logs[idx]["type"]],
                        title: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: NeuText(controller.logs[idx]["msg"],
                                  size: 10),
                            ))));
              },
            )))
          ]),
        ));
  }
}
