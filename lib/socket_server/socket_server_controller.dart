import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:socket_io/socket_io.dart';

import 'message_service.dart';

import 'package:get/get.dart';

enum LogType { info, warning, error, success }

class ClientIds {
  Set<String> clientIds = {};

  registId(String id) {
    clientIds.add(id);
  }

  removeId(String id) {
    clientIds.remove(id);
  }
}

class SocketServerController extends GetxController {
  final LOG_LIMIT = 300;
  late Server server;
  late bool isStartup = false;
  List<Map<String, dynamic>> logs = [];
  ScrollController scrollController = ScrollController();
  Map<String, ClientIds> accountMap = {};
  Map<String, String> clientMap = {};
  @override
  onInit() async {
    super.onInit();
    if (!isStartup) {
      await startup();
    }
  }

  @override
  onClose() {
    close();
  }

  void addLogs(String message, [LogType type = LogType.info]) {
    logs.insert(0, {"msg": "${DateTime.now()} --> $message", "type": type});
    if (logs.length > LOG_LIMIT) {
      logs.removeLast();
    }
    if (scrollController.hasClients) {
      scrollController.animateTo(0,
          duration: const Duration(milliseconds: 300), curve: Curves.linear);
    }
    update();
  }

  Future<void> startup() async {
    server = Server();
    //var nsp = server.of('/myNameSpace');
    server.on('connection', (client) {
      on_userConnect(client);
      on_disConnect(client);
      on_sendMessage(client);
    });
    await server.listen(8765);
    addLogs("server服务已启动!!", LogType.success);
    isStartup = true;
  }

  Future<void> close() async {
    await server.close();
    isStartup = false;
    addLogs("socket服务已关闭!!");
  }

  List<int> encode(dynamic data) {
    return utf8.encode(json.encode(data));
  }

  dynamic decode(List<int> data) {
    return json.decode(utf8.decode(data));
  }

  on_userConnect(client) {
    client.on('userConnect', (bdata) async {
      final data = decode(bdata);
      addLogs(
          "${data['account']} has connected as ${client.id}", LogType.success);
      if (accountMap[data['account']] == null) {
        accountMap[data['account']] = ClientIds();
      }
      accountMap[data['account']]!.registId(client.id);
      clientMap[client.id] = data['account'];
      //client.broadcast.emitWithBinary("newConnect", encode("new user ${client.id} connected."));
    });
  }

  on_disConnect(client) {
    client.on('disconnect', (reason) {
      addLogs("${client.id} disconnected.", LogType.warning);
      final account = clientMap[client.id];
      accountMap[account]?.removeId(client.id);
      clientMap.remove(client.id);
      // client.broadcast
      //     .emitWithBinary("newDisconnect", enocde("user $account[${client.id}] disconnected."));
    });
  }

  on_sendMessage(client) {
    client.on('sendMessage', (dataList) async {
      final data = decode((dataList as List).first);
      final msg = data['msg'];
      final toAccount = data['toAccount'];
      final toClientIds = accountMap[toAccount];
      addLogs("$msg to $toAccount", LogType.info);
      final account = clientMap[client.id];
      final ack = dataList.last;
      final res =
          "$msg had been sent!,${accountMap[toAccount]?.clientIds.length}";
      if (toClientIds != null) {
        for (int idx = 0; idx < toClientIds.clientIds.length; idx++) {
          final backMsg = {'msg': msg, 'fromAccount': account};
          client
              .to(toClientIds.clientIds.toList()[idx])
              .emitWithBinary('recieveMessage', encode(backMsg));
        }
      }
      ack(res);
    });
  }
}
